<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'fbsfE9c_FSY:APA91bFrgNoIhB4KV4i9Fan9L0_Mbqyo_gBbI33aL_CTa08lQbMwR0yHuL5wUv25kSlF2kWNgCg-yk8KmYrdKRct7nqNiQD7XswXGJojMHl3ddTs8yS38bGYWcY2E4DqJSxPFHlzsBnI',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
